#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<int> v1 = { 1, 1, 1, 1, 1 };

    for (int i = 0; i < v1.size(); i++)
    {
        cout << v1[i] << " ";
    }

    cout << endl;

    vector<int>::iterator it;

    for (it = v1.begin(); it != v1.end(); ++it) {
        *it = 0;
    }

    for (int i = 0; i < v1.size(); i++)
    {
        cout << v1[i] << " ";
    }

    return 0;
}